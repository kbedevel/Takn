var phones =
[    
    {"type":"Fijo","format":"+56 2"},
    {"type":"Móvil","format":"+56 9"}
];
function fillSelectPhoneType(){
    phones.forEach(e =>{
        $('#select-phone-type').append(
            '<option name="'+e.type+'" value="'+e.format+'">'+e.type+'</option>'
        );
    });
};
function fillSelectPhoneType1(){
    phones.forEach(e =>{
        $('#select-phone-type1').append(
            '<option name="'+e.type+'" value="'+e.format+'">'+e.type+'</option>'
        );
    });
};
function fillSelectPhoneType2(){
    phones.forEach(e =>{
        $('#select-phone-type2').append(
            '<option name="'+e.type+'" value="'+e.format+'">'+e.type+'</option>'
        );
    });
};
function fillSelectPhoneType3(){
    phones.forEach(e =>{
        $('#select-phone-type3').append(
            '<option name="'+e.type+'" value="'+e.format+'">'+e.type+'</option>'
        );
    });
};
function fillInputPhoneType(){
    var phoneTypeValue = $('#select-phone-type').val();
    var phoneTypeFormat;
    phones.forEach(e => {        
        if(e.format == phoneTypeValue){
            phoneTypeFormat = e.format;
        }else{
            return false;
        }
    });
    document.getElementById('input-phone-type').value = phoneTypeFormat;
};
function fillInputPhoneType1(){
    var phoneTypeValue = $('#select-phone-type1').val();
    var phoneTypeFormat;
    phones.forEach(e => {        
        if(e.format == phoneTypeValue){
            phoneTypeFormat = e.format;
        }else{
            return false;
        }
    });
    document.getElementById('input-phone-type1').value = phoneTypeFormat;
};
function fillInputPhoneType2(){
    var phoneTypeValue = $('#select-phone-type2').val();
    var phoneTypeFormat;
    phones.forEach(e => {        
        if(e.format == phoneTypeValue){
            phoneTypeFormat = e.format;
        }else{
            return false;
        }
    });
    document.getElementById('input-phone-type2').value = phoneTypeFormat;
};
function fillInputPhoneType3(){
    var phoneTypeValue = $('#select-phone-type3').val();
    var phoneTypeFormat;
    phones.forEach(e => {        
        if(e.format == phoneTypeValue){
            phoneTypeFormat = e.format;
        }else{
            return false;
        }
    });
    document.getElementById('input-phone-type3').value = phoneTypeFormat;
};
$(document).ready(function(){
    fillSelectPhoneType();
    fillInputPhoneType();
    fillSelectPhoneType1();
    fillInputPhoneType1();
    fillSelectPhoneType2();
    fillInputPhoneType2();
    fillSelectPhoneType3();
    fillInputPhoneType3();
});
$('#select-phone-type').on('change', function(){
    fillInputPhoneType();
});
$('#select-phone-type1').on('change', function(){
    fillInputPhoneType1();
});
$('#select-phone-type2').on('change', function(){
    fillInputPhoneType2();
});
$('#select-phone-type3').on('change', function(){
    fillInputPhoneType3();
});