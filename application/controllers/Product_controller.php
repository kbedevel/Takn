<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product_controller extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Product_model');
        $this->load->model('Order_model');
    }

    public function cart(){
        if($this->session->id!==null){
            $item = [
                "ORDER_PRODUCT_QUANTITY"   => $this->input->post('input-quantity'),
                "ORDER_PRODUCT_PRODUCT_ID" => $this->input->post('input-product-id')
            ];
            $this->session->cart += $item;
            redirect('/User_controller/catalogue', 'refresh');
        }else{
            $this->session->set_flashdata('msg', 'No has iniciado sesión');
            redirect('/User_controller', 'refresh');
        }
    }

    public function deleteCartElement($id){
        array_diff($this->session->cart, $id);
    }

    public function createOrder($cart){
        $mount = 0;
        foreach($cart as $key => $c){
            $mount += ($this->Order_model->read($c['ORDER_PRODUCT_PRODUCT_ID'])['PRODUCT_PRICE'])*$c['ORDER_PRODUCT_QUANTITY'];
        }
        $order = [
            "T_USER_ORDER_T_USER_ID"  => $this->session->id,
            "T_USER_ORDER_PLACE_ID"   => $_POST['select-place'],
            "T_USER_ORDER_PAY_METHOD" => 0,
            "T_USER_ORDER_MOUNT"      => $mount
        ];
        $this->session->set_userdata('temp_order', $order);
        $this->Order_model->createOrder($order);
        $order_products = [];
        $last_order = $this->Order_model->selectLastOrder($this->session->id);
        foreach($cart as $key => $c){
            $order_products += [
                "ORDER_PRODUCT_ORDER_ID"   => $last_order,
                "ORDER_PRODUCT_QUANTITY"   => $c['ORDER_PRODUCT_QUANTITY'],
                "ORDER_PRODUCT_PRODUCT_ID" => $c['ORDER_PRODUCT_PRODUCT_ID']
            ];
        }
        foreach($order_products as $key => $op){
            $this->Order_model->create($op);
        }
        redirect('/User_controller/orders', 'refresh');
    }

    public function product($id){       
        if($id > count($this->Product_model->getAll())){
            $product['product'] = null;
            //$this->load->helper('main');
            $this->load->view('product_view', $product);
        }else{
            $product['product'] = $this->Product_model->read($id);
            $product['comments'] = $this->Product_model->readComments($id);
            //$this->load->helper('main');
            $this->load->view('product_view', $product);
            //echo json_encode($product['comments']);
        }
    }

    public function commentProduct($product_id){
        $comment = [
            "T_USER_COMMENT_CONTENT"    => $this->input->post('input-comment'),
            "T_USER_COMMENT_T_USER_ID"  => $this->session->id,
            "T_USER_COMMENT_PRODUCT_ID" => $product_id
        ];
        $this->Product_model->insertComment($comment);
        $adr = '/Product_controller/product/'.$product_id;
        redirect($adr, 'refresh');
    }

    public function adminProducts(){
        $user['user'] = $this->User_model->readUserById($this->session->id);
        if($user['user'][0]['T_USER_TYPE']==1){
            $this->load->view('update_catalogue_view');
        }else{
            echo '<h1>Error 503: Acceso denegado</h1><br><a href="<?=base_url();?>index.php/User_controller/catalogue">Regresar al catálogo </a>';
            redirect('/User_controller','refresh');
        }
    }
    public function addProduct(){        
        $product = [
            "PRODUCT_IMAGE"         =>  $this->input->post('input-url'),
            "PRODUCT_NAME"          =>  $this->input->post('input-name'),
            "PRODUCT_CATEGORY"      =>  $this->input->post('input-category'),
            "PRODUCT_DESCRIPTION"   =>  $this->input->post('textarea-description'),
            "PRODUCT_ORIGIN"        =>  $_POST['select-countries'],
            "PRODUCT_PRICE"         =>  $this->input->post('input-price'),
            "PRODUCT_STOCK"         =>  $_POST['select-stock']
        ];
        if($product != null){
            $this->Product_model->create($product);
        }
        redirect('/Product_controller/adminProducts','refresh');
    }

    public function index(){
        redirect('/User_controller/catalogue','refresh');
    }
}
?>
