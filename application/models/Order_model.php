<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order_model extends CI_Model {
    //PRODUCT_MODEL CONSTRUCTOR
	public function __construct()
	{
        parent::__construct();
        $this->load->database();
    }
    
    //CRUD
    public function create($order_product){
        return $this->db->insert('ORDER_PRODUCT', $order_product);
    }
    public function read($id){        
        $this->db->from('ORDER_PRODUCT');
        $this->db->join('PRODUCT','PRODUCT.PRODUCT_ID = ORDER_PRODUCT.ORDER_PRODUCT_PRODUCT_ID');
        $this->db->join('T_USER_ORDER','T_USER_ORDER.T_USER_ORDER_ID = ORDER_PRODUCT.ORDER_PRODUCT_ORDER_ID');
        $this->db->join('PLACE','PLACE.PLACE_ID= T_USER_ORDER.T_USER_ORDER_PLACE_ID');
        $this->db->where('T_USER_ORDER.T_USER_ORDER_T_USER_ID', $id); 
        return $query = $this->db->get()->result_array();
    }
    public function update($id,$order){
        $this->db->where('ORDER_PRODUCT_ID',$id);
        return $this->db->update("ORDER_PRODUCT",$order);
    }
    public function delete($id){
        $this->db->where('ORDER_PRODUCT_ID',$id);
        return $this->db->delete('ORDER_PRODUCT');
    }
    public function getAll(){
        return $this->db->get('ORDER_PRODUCT')->result_array();
    }
    public function searchProduct($id){
        $this->db->from('PRODUCT');
        $this->db->join('ORDER_PRODUCT','ORDER_PRODUCT.ORDER_PRODUCT_PRODUCT_ID = PRODUCT.PRODUCT_ID');
        $this->db->where('ORDER_PRODUCT.ORDER_PRODUCT_PRODUCT_ID', $id); 
        return $this->db->get()->result_array();
    }
    //END CRUD
    public function getUserOrders($user_id){
        $this->db->where('T_USER_ORDER_T_USER_ID', $user_id);
        return $this->db->get('T_USER_ORDER')->result_array();
    }
    public function createOrder($order){
        return $this->db->insert('T_USER_ORDER', $order);
    }
    public function selectLastOrder($user_id){
        return $this->db->query('SELECT T_USER_ORDER_ID FROM T_USER_ORDER WHERE T_USER_ORDER_T_USER_ID = $user_id ORDER BY T_USER_ORDER_T_USER_ID DESC LIMIT 1');
    }
}
?>