<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends CI_Model {
    //User_Model CONSTRUCTOR
	public function __construct()
	{
        parent::__construct();
        $this->load->database();
    }
    
    //LOGIN METHOD
    public function login($email, $password){
        $this->db->where('T_USER_EMAIL', $email);
        $this->db->where('T_USER_PASSWORD', $password);
        return $this->db->get('T_USER')->result_array();        
    }
    //END LOGIN
    
    //CRUD
    public function create($user){
        return $this->db->insert('T_USER', $user);
    }
    public function read($email){        
        $this->db->where('T_USER_EMAIL',$email);
        return $this->db->get('T_USER')->result_array();
    }
    public function update($id,$user){
        $this->db->where('T_USER_ID',$id);
        return $this->db->update('T_USER',$user);
    }
    public function delete($email){
        $this->db->where('T_USER_EMAIL',$email);
        return $this->db->delete('T_USER');
    }
    public function getAll(){
        return $this->db->get('T_USER')->result_array();
    }
    //END CRUD
    //NEWSLETTER
    public function createEmail($email){
        return $this->db->insert('NEWSLETTER', $email);
    }
    //
    public function readUserById($id){        
        $this->db->where('T_USER_ID',$id);
        return $this->db->get('T_USER')->result_array();
    }
}
?>