<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart_model extends CI_Model {
	public function __construct(){
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Product_model');
    }
?>