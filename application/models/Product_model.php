<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product_model extends CI_Model {
    //PRODUCT_MODEL CONSTRUCTOR
	public function __construct()
	{
        parent::__construct();
        $this->load->database();
    }
    
    //CRUD
    public function create($product){
        return $this->db->insert('PRODUCT', $product);
    }
    public function read($id){        
        $this->db->where('PRODUCT_ID',$id);
        return $this->db->get('PRODUCT')->result_array();
    }
    public function update($id,$product){
        $this->db->where('PRODUCT_ID',$id);
        return $this->db->update("PRODUCT",$product);
    }
    public function delete($id){
        $this->db->where('PRODUCT_ID',$id);
        return $this->db->delete('PRODUCT');
    }
    public function getAll(){
        return $this->db->get('PRODUCT')->result_array();
    }
    //END CRUD
    public function readComments($id){       
        $this->db->select('*'); 
        $this->db->from('T_USER_COMMENT');
        $this->db->join('T_USER', 'T_USER.T_USER_ID = T_USER_COMMENT.T_USER_COMMENT_T_USER_ID');
        $this->db->where('T_USER_COMMENT_PRODUCT_ID', $id);
        return $this->db->get()->result_array();
    }
    public function insertComment($comment){
        return $this->db->insert('T_USER_COMMENT', $comment);
    }

    public function deleteComment($comment_id){
        $this->db->where('T_USER_COMMENT_ID', $comment_id);
        return $this->db->delete('T_USER_COMMENT');
    }
}
?>