<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Place_model extends CI_Model {
    //PRODUCT_MODEL CONSTRUCTOR
	public function __construct()
	{
        parent::__construct();
        $this->load->database();
    }    
    //CRUD
    public function create($place){
        return $this->db->insert('PLACE', $place);
    }
    public function read($id){        
        $this->db->where('PLACE_T_USER_ID',$id);
        return $this->db->get('PLACE')->result_array();
    }
    public function update($id,$order){
        $this->db->where('PLACE_ID',$id);
        return $this->db->update("PLACE",$order);
    }
    public function delete($id){
        $this->db->where('PLACE_ID',$id);
        return $this->db->delete('PLACE');
    }
    public function getAll(){
        return $this->db->get('PLACE')->result_array();
    }
    //END CRUD
}
?>