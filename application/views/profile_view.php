<!doctype html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>TaKn | Página principal </title>
  <link href="<?=base_url()?>resources/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
  <!-- Custom fonts -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  <link href="<?=base_url()?>resources/css/fontawesome-all.css" rel="stylesheet">
  <!-- Plugin CSS -->
  <link href="<?=base_url()?>resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
  <!-- Custom -->
  <link href="<?=base_url()?>resources/css/creative.css" rel="stylesheet">
  <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
  <!-- Bootstrap core JavaScript -->
  <script src="<?=base_url()?>resources/vendor/jquery/jquery.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Plugin JavaScript -->
  <script src="<?=base_url()?>resources/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/scrollreveal/scrollreveal.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
  <!-- Custom scripts for this template -->
  <script defer src="<?=base_url()?>resources/js/creative.js"></script>
</head>

<body id="page-top">
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">TaKn<small class="nav-append">&nbsp;It's tasty</small></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
           <a class="nav-link js-scroll-trigger" href="/index.php/Welcome">INICIO</a>
          </li>
          <li class="nav-item">
           <a class="nav-link js-scroll-trigger" href="/index.php/User_controller/catalogue">CATÁLOGO</a>
          </li>          
          <li class="nav-item">
           <a class="nav-link js-scroll-trigger" href="#">|</a>
          </li>
          <?php echo
            '<li class="dropdown nav-drop nav-item">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown">'.$this->session->complete_name.'&nbsp;&nbsp;<img style="max-width:22px;" src="'.$this->session->user_image.'"/><b class="caret"></b></span></a>           
              <ul class="dropdown-menu">
                <li><a href="/index.php/User_controller/profiler">Perfil</a></li>
                <li><a href="/index.php/User_controller/places">Mis Lugares</a></li>
                <li><a href="/index.php/User_controller/orders">Mis Órdenes</a></li>
                <li><a href="/index.php/User_controller/closeSession">Cerrar Sesión</a></li>
              </ul>
            </li>';
          ?>
        </ul>
      </div>
    </div>
  </nav>

  <header class="masthead text-center text-white d-flex">
   <div class="container my-auto" id="div-profile">
    <div class="row" style="padding: 50px;">
      <div class="col col-lg-4">
        <img style="width: 150px;" src="<?php echo $this->session->user_image;?>"/>
      </div>
      <div class="col col-lg-8">
        <br>
        <h1 class="text-left"><?php echo $this->session->complete_name;?></h1>
      </div>
    </div>
    <div class="row" style="padding: 15px;">
      <div class="col-12 col-lg-" style="color: rgba(255, 255, 255, 0.7);">
        <h4 class="text-center">Su ID: <?php echo strtoupper($this->session->id);?></h4>
      </div>
    </div>
    <div class="row" style="padding: 15px; padding-bottom: 50px;">
      <div class="col-12 col-lg" style="color: rgba(255, 255, 255, 0.7);">
        <h4 class="text-center">Email: <?php echo $this->session->mail;?></h4>
      </div>      
    </div>
    <div>
    <a href="<?=base_url();?>index.php/User_controller/editor" class="btn btn-light btn-lg active" role="button" aria-pressed="true">Editar</a>
    </div>        
   </div>   
  </header>

  <section id="footer" class="bg-dark text-white">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <h2 class="section-heading">Gracias por preferirnos</h2>
        </div>
      </div>
    </div>
  </section>

  <div class="container-fluid footer">
    <div class="row">
      <div class="container">
        <div class="row div-copyright-footer">
          <div class="col-lg text-center">
            <a href="/index.php/Welcome">TaKn</a>&nbsp;©&nbsp;2018&nbsp;-&nbsp;Todos los derechos reservados
          </p>
        </div>
      </div>
    </div>
  </div>
</body>
</html>