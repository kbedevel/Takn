<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>TaKn | It's tasty</title>
    <link href="<?=base_url()?>resources/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="<?=base_url()?>resources/css/fontawesome-all.css" rel="stylesheet">
    <!-- Plugin CSS -->
    <link href="<?=base_url()?>resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <!-- Custom -->
    <link href="<?=base_url()?>resources/css/creative.css" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url()?>resources/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url()?>resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="<?=base_url()?>resources/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?=base_url()?>resources/vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="<?=base_url()?>resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Custom scripts for this template -->
    <script defer src="<?=base_url()?>resources/js/creative.js"></script>
  </head>     
  <body id="page-top">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">TaKn<small class="nav-append">&nbsp;It's tasty</small></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
          <?php if($this->session->admin_prop == 1){ echo 
          '<li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/index.php/Product_controller/adminProducts">Administracion de productos</a>
          </li>';
          }
          ?>                      
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="/index.php/User_controller/catalogue">Catálogo</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Servicios</a>
            </li>
            <?php if(strlen($this->session->id)<1){ echo
              '<li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#newsletter">Newsletter</a>
              </li>';
            }
            ?>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li>
            <li class="nav-item sm-hidden">
              <a class="nav-link js-scroll-trigger">|</a>
            </li>
            <?php if(strlen($this->session->id)<1){ echo
            '<li class="nav-item"><a class="nav-link js-scroll-trigger" href="/index.php/User_controller">INGRESAR</a></li>';
            }else{ echo
            '<li class="dropdown nav-drop nav-item">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown">'.$this->session->complete_name.'&nbsp;&nbsp;<img style="max-width:22px;" src="'.$this->session->user_image.'"/><b class="caret"></b></span></a>           
              <ul class="dropdown-menu">
                <li><a href="/index.php/User_controller/profiler">Perfil</a></li>';
                if($this->session->admin_prop == 2){ echo '
                  <li><a href="/index.php/User_controller/places">Mis Lugares</a></li>
                <li><a href="/index.php/User_controller/orders">Mis Órdenes</a></li>
                ';}echo '                                
                <li><a href="/index.php/User_controller/closeSession">Cerrar Sesión</a></li>
              </ul>
            </li>';
            }?>
          </ul>
        </div>
      </div>
    </nav>

    <div class="div-background-video">
      <video id="video-food" preload="auto" autoplay="true" loop="loop" muted="muted">
        <source src="<?=base_url();?>resources/video/background_video.mp4" type="video/mp4">
      </video>
    </div>

    <header class="masthead text-center text-white d-flex" id="header-main">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>Descubre algo único</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="mb-5"><strong>Con TaKn podrás explorar un mundo nuevo de sabores y aromas.</strong></p>
            <?php if(strlen($this->session->id)<1){ echo
              '<a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Cuéntame más</a>';
            }else{ echo
              '<a class="btn btn-primary btn-xl js-scroll-trigger" href="/index.php/User_controller/catalogue">Ir</a>';
            }?>
          </div>
        </div>
      </div>
    </header>
    
    <?php if(strlen($this->session->id)<1){ echo
      '<section class="bg-primary" id="about">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto text-center">
              <h2 class="section-heading text-white">No te pierdas esta exquisita experiencia</h2>
              <hr class="light my-4">
              <p class="text-faded mb-4">Con un menú de múltiples platillos, TaKn busca reunir la mejor experiencia culinaria donde te parezca más cómodo. Regístrate para descubrir lo que tenemos para ti.</p>
              <a class="btn btn-light btn-xl js-scroll-trigger" href="/index.php/User_controller/register_page">Quiero registrarme</a>
            </div>
          </div>
        </div>
      </section>';
    }?>

    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Nuestros servicios</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-home text-primary mb-3"></i>
              <h3 class="mb-3">Despacho a domicilio</h3>
              <p class="text-muted mb-0">Disfruta de lo mejor en la comodidad de tu hogar.</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-store text-primary mb-3"></i>
              <h3 class="mb-3">Retira en una tienda</h3>
              <p class="text-muted mb-0">La mejor opción si vas de prisa y estás pasando cerca.</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-newspaper text-primary mb-3"></i>
              <h3 class="mb-3">Newsletter</h3>
              <p class="text-muted mb-0">Renovamos nuestra información continuamente, para que disfrutes de un menú fresco en todos los sentidos.</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-utensils text-primary mb-3"></i>
              <h3 class="mb-3">Banquetería On Demand</h3>
              <p class="text-muted mb-0">Puedes obtener un descuento especial si solicitas el despacho banquete.</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php if(strlen($this->session->id)<1){ echo
      '<section id="newsletter" class="bg-gray text-white">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto text-center">
              <h2 class="section-heading">Suscríbete al Newsletter</h2>
              <hr class="my-4">
              <p class="mb-5"></p>
            </div>
          </div>
          <form id="form-newsletter" method="post" action="/index.php/User_controller/newsletter">
          <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
            <div class="form-group col-lg mx-auto">
              <input name="input-newsletter-email" type="email" class="form-control" placeholder="Ingresa tu correo electrónico"><br><br>
            </div>
            </div>
            <div class="col-lg-3"></div>
          </div>
          <div class="row">
            <div class="col-2 col-lg"></div>
            <div class="col col-lg">
              <a class="btn btn-dark btn-xl col-12 js-scroll-trigger" href="#">Suscribirme</a>
            </div>
            <div class="col-2 col-lg"></div>
          </div>
        </div>
      </section>';
    }?>

    <section id="contact" class="bg-dark text-white">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Mantengámonos comunicados</h2>
            <hr class="my-4">
            <p class="mb-5"></p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 ml-auto text-center">
            <i class="fas fa-phone fa-3x mb-3"></i>
            <p>300 800 5522&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;300 800 6699</p>
          </div>
          <div class="col-lg-4 mr-auto text-center">
            <i class="fas fa-envelope-open fa-3x mb-3"></i>
            <p><a href="mailto:your-email@your-domain.com">ayuda@takn.cl</a></p>
          </div>
        </div>
      </div>
    </section>

    <div class="container-fluid footer">
      <div class="row">
        <div class="container">
          <div class="row div-copyright-footer">
            <div class="col-lg text-center">
              <a href="/index.php/Welcome">TaKn</a>&nbsp;©&nbsp;2018&nbsp;-&nbsp;Todos los derechos reservados
            </p>
          </div>
        </div>
      </div>
    </div>    
  </body>
</html>