<!doctype html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="assets/img/favicon.ico?v=1.1">
  <title>TaKn | Mis Lugares </title>
  <link href="<?=base_url()?>resources/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
  <!-- Custom fonts -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  <link href="<?=base_url()?>resources/css/fontawesome-all.css" rel="stylesheet">
  <!-- Plugin CSS -->
  <link href="<?=base_url()?>resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
  <!-- Custom -->
  <link href="<?=base_url()?>resources/css/creative.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link href="<?base_url();?>resources/images/">
  <script defer src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
  <!-- Bootstrap core JavaScript -->
  <script src="<?=base_url()?>resources/vendor/jquery/jquery.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Plugin JavaScript -->
  <script src="<?=base_url()?>resources/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/scrollreveal/scrollreveal.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
  <!-- Custom scripts for this template -->
  <script defer src="<?=base_url()?>resources/js/creative.js"></script>
</head>

<body id="page-top">
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">TaKn<small class="nav-append">&nbsp;It's tasty</small></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/index.php/welcome">INICIO</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#">|</a>
          </li>
          <li class="dropdown nav-drop nav-item">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown"><?= $this->session->complete_name.'&nbsp;&nbsp;<img style="max-width:22px;" src="'.$this->session->user_image.'"/><b class="caret"></b></span></a>            
            <ul class="dropdown-menu">
              <li><a href="/index.php/User_controller/profiler">Perfil</a></li>
              <li><a href="/index.php/User_controller/places">Mis Lugares</a></li>
              <li><a href="/index.php/User_controller/orders">Mis Órdenes</a></li>
              <li><a href="/index.php/User_controller/closeSession">Cerrar Sesión</a></li>
            </ul>';?>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <header class="masthead text-center text-white d-flex">
    <div class="container my-auto">
    <?php if(count($places)<5){ echo
      '<h1>Registra tu lugar</h1><br>
      <form id="form-place" method="post" action="/index.php/User_controller/register_place">
      <div class="form-row">
        <div class="form-group col">
          <input type="text" class="form-control" name="input-place-name" id="input-place-name" maxlength="20" placeholder="Ponle un nombre a tu lugar" required>
        </div>        
      </div>
      <div class="form-row">
        <div class="form-group col-6">
          <input type="text" class="form-control" name="input-place-address" id="input-place-address" maxlength="50" placeholder="Dirección" required>
        </div>
        <div class="form-group input-group col-3">
          <input type="text" class="form-control col-4 col-lg-2" name="input-place-building-number-label" id="input-place-building-number-label" value="#"disabled>
          <input type="text" class="form-control" name="input-place-building-number" id="input-place-building-number" title="Numero de la Dirección"
            maxlength="5" placeholder="Número de edificio" required>
        </div>
        <div class="form-group col-3">
          <input type="text" class="form-control" name="input-place-department" id="input-place-department" title="Ignora si no aplica." maxlength="5" placeholder="Número de departamento">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-4">
          <select id="select-regions" name="select-regions" class="form-control" required></select>
        </div>
        <div class="form-group col-4">
          <select id="select-districts" name="select-districts" class="form-control" required></select>
        </div>
        <div class="form-group col-4">
          <input type="text" class="form-control" name="input-place-postal-code" id="input-place-postal-code" maxlength="10" placeholder="Código postal" required>      
        </div>
      </div>
      <div class="form-row">
        <div class="col-2 col-lg-5"></div>        
        <button type="submit" class="btn btn-primary btn-xl col-8 col-lg-2" name="button-complete-register" id="button-complete-register">Guardar</button>        
        <div class="col-2 col-lg-5"></div>
      </div>';}else{ echo
        '<div class="col-lg-10 mx-auto">
          <h1 class="text-uppercase">
            <strong>Ouch, ya no puedes agregar más lugares, has alcanzado el límite de los que puedes agregar.</strong>
          </h1>
          <hr>
          <div class="col-lg-8 mx-auto">
            <p class="mb-5"><strong>Recuerda que sólo tienes un máximo de 5 lugares para guardar. Pero no te preocupes, siempre puedes modificarlos si estás en otra parte.</strong></p>
          </div>
        </div>';
      }?>       
    </div>    
  </header>
  </form>

  <section class="bg-primary" id="about">
    <div class="container">
      <div class="row" id="div-places-container">
        <div class="col-lg-8 mx-auto text-center">
          <h2 class="section-heading text-white">Mis lugares guardados</h2>
          <hr class="light my-4">       
        <?php if(count($places)==0){ echo        
          '<p class="text-faded mb-4">Parece que todavía no has agregado un lugar.</p>
        </div>';
        }else{ echo
        '<table id="table-my-places">
          <tr>
            <th>Nombre</th>
            <th>Localización</th>
            <th></th>
            <th></th>
          </tr>';
          foreach($places as $key => $l){            
            echo
            '<tr>
              <td>'.$l['PLACE_NAME'].'</td>
              <td>'.$l['PLACE_ADDRESS'].' #'.$l['PLACE_BUILDING_NUMBER'].'&nbsp;'.$l['PLACE_DEPARTMENT'].'</td>
              <td><a name="a-modify" href="">Modificar</a></td>
              <td><a name="a-delete" href="">Eliminar</a></td>
            </tr>';              
          } echo 
            '</table>
          </div>';     
        }?>
      </div>      
    </div>
  </section>

  <section id="footer" class="bg-dark text-white">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <h2 class="section-heading">Gracias por preferirnos</h2>
        </div>
      </div>
    </div>
  </section>

  <div class="container-fluid footer">
    <div class="row">
      <div class="container">
        <div class="row div-copyright-footer">
          <div class="col-lg text-center">
            <a href="/index.php/Welcome">TaKn</a>&nbsp;©&nbsp;2018&nbsp;-&nbsp;Todos los derechos reservados
          </p>
        </div>
      </div>
    </div>
  </div>
  
  <script defer >    
    $(document).ready( function () {
      $('#table-my-places').DataTable();
    } );
  </script>
</body>
</html>