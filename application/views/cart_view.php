<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>TaKn | Carro</title>  
  <link href="<?=base_url()?>resources/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
  <!-- Custom fonts -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  <link href="<?=base_url()?>resources/css/fontawesome-all.css" rel="stylesheet">
  <!-- Plugin CSS -->
  <link href="<?=base_url()?>resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
  <!-- Custom -->
  <link href="<?=base_url()?>resources/css/creative.css" rel="stylesheet">
  <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
  <!-- Bootstrap core JavaScript -->
  <script src="<?=base_url()?>resources/vendor/jquery/jquery.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Plugin JavaScript -->
  <script src="<?=base_url()?>resources/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/scrollreveal/scrollreveal.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
  <!-- Custom scripts for this template -->
  <script defer src="<?=base_url()?>resources/js/creative.js"></script>
</head>

<body id="page-top">
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">TaKn
        <small class="nav-append">&nbsp;It's tasty</small>
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/index.php/welcome">Inicio</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger">|</a>
          </li>        
          <?php if(strlen($this->session->id)<1){ echo
          '<li class="nav-item"><a class="nav-link js-scroll-trigger" href="/index.php/User_controller">Iniciar Sesión</a></li>';
          }else{ echo
          '<li class="dropdown nav-drop nav-item">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown">'.$this->session->complete_name.'&nbsp;&nbsp;<img style="max-width:22px;" src="'.$this->session->user_image.'"/><b class="caret"></b></span></a>            
            <ul class="dropdown-menu">
              <li><a href="/index.php/User_controller/profiler">Perfil</a></li>';
              if($this->session->admin_prop == 2){ echo '
                <li><a href="/index.php/User_controller/places">Mis Lugares</a></li>
              <li><a href="/index.php/User_controller/orders">Mis Órdenes</a></li>
              ';}echo '
              <li><a href="/index.php/User_controller/closeSession">Cerrar Sesión</a></li>
            </ul>
          </li>';
          }?>        
        </ul>
      </div>
    </div>
  </nav>

  <header class="masthead text-center text-white d-flex">
    <div class="container header-catalogue my-auto">
    <form action="<?=base_url()?>index.php/User_controller/register" method="post">
      <div class="row" style="padding-top:110px;">
      <?php foreach($orders as $key=>$l){ echo 
        '<div class="col-6 col-lg-3">
          <div class="card border-dark mb-3 card-catalogue">          
          <form id="form-product" action="'.base_url().'index.php/Product_controller/cart" method="POST">
            <a href="'.base_url().'index.php/Product_controller/product/'.$l["ORDER_PRODUCT_PRODUCT_ID"].'">';
      }?>
      <?php foreach($products as $key => $p){
        '<img class="card-img-top" src="'.$p["PRODUCT_IMAGE"].'"/></a>
            <input type="text" name="input-product-id" value="'.$l["ORDER_PRODUCT_PRODUCT_ID"].'" hidden>
            <div class="card-body">                            
              <h5 class="card-title">'.$p["PRODUCT_NAME"].'</h5>
              <small class="card-text">CLP$ '.number_format($p["PRODUCT_PRICE"]).'</small>
              <div style="padding-bottom:10px;"></div>
              </div>
            </form>          
          </div>                
        </div>';
      }?>      
      </div>
      </form>
      <div class="card-footer">                                 
              <button style="position:relative;" class="btn btn-light btn-md form-control card-text js-scroll-trigger" type="submit">Comprar</button>
            </div>
    </div>
  </header>

  <section id="section-under-catalogue" class="bg-dark">    
  </section>

  <section id="contact" class="bg-dark text-white">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <h2 class="section-heading">Mantengámonos comunicados</h2>
          <hr class="my-4">
          <p class="mb-5"></p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 ml-auto text-center">
          <i class="fas fa-phone fa-3x mb-3"></i>
          <p>300 800 5522&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;300 800 6699</p>
        </div>
        <div class="col-lg-4 mr-auto text-center">
          <i class="fas fa-envelope-open fa-3x mb-3"></i>
          <p>
            <a href="mailto:your-email@your-domain.com">ayuda@takn.cl</a>
          </p>
        </div>
      </div>
    </div>
  </section>

  <div class="container-fluid footer">
    <div class="row">
      <div class="container">
        <div class="row div-copyright-footer">
          <div class="col-lg text-center">
            <p><a href="/index.php/Welcome">TaKn</a>&nbsp;©&nbsp;2018&nbsp;-&nbsp;Todos los derechos reservados</p>
          </div>
        </div>
      </div>
    </div> 
  </div>    

  <?php
    if($this->session->msg != null){
      echo "<script>
              if(confirm('".$this->session->msg."')){
                window.location.href=('".base_url()."index.php/User_controller');
              }else{
                return false;
              }
            </script>";
      $this->session->set_userdata("msg",null);
    }    
  ?>
  <script type="text/javascript" language="javascript">
    function appendCart(id){
      var ide = '#'+'input-quantity-'+id;
      var productQuantity = $(ide).val;
      var product = {"ORDER_PRODUCT_PRODUCT_ID" : id, 
                     "ORDER_PRODUCT_QUANTITY"   : productQuantity};        
      }
      return false;
    }
  </script>
</body>
</html>