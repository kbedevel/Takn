<!doctype html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>TaKn | Registro </title>
  <link href="<?=base_url()?>resources/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
  <!-- Custom fonts -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
    rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
    rel='stylesheet' type='text/css'>
  <link href="<?=base_url()?>resources/css/fontawesome-all.css" rel="stylesheet">
  <!-- Plugin CSS -->
  <link href="<?=base_url()?>resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
  <!-- Custom -->
  <link href="<?=base_url()?>resources/css/creative.css" rel="stylesheet">


  <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe"
    crossorigin="anonymous"></script>
  <!-- Bootstrap core JavaScript -->
  <script src="<?=base_url()?>resources/vendor/jquery/jquery.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Plugin JavaScript -->
  <script src="<?=base_url()?>resources/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/scrollreveal/scrollreveal.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
  <!-- Custom scripts for this template -->
  <script defer src="<?=base_url()?>resources/js/creative.js"></script>
  <script defer src="<?=base_url()?>resources/js/places.js"></script>
</head>

<body id="page-top">
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">TaKn
        <small class="nav-append">&nbsp;It's tasty</small>
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">          
        <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/index.php/welcome">INICIO</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#faq">FAQ</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">CONTACT</a>
          </li>
          <?php if(strlen($this->session->id)<1){ echo
              '<li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#newsletter">Newsletter</a>
              </li>';
            }            
            ?>
            <li class="nav-item sm-hidden">
              <a class="nav-link js-scroll-trigger">|</a>
            </li>            
            <?php if(strlen($this->session->id)<1){ echo
            '<li class="nav-item"><a class="nav-link js-scroll-trigger" href="/index.php/User_controller">INGRESAR</a></li>';
            }else{ echo
            '<li class="dropdown nav-drop nav-item">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown">'.$this->session->complete_name.'&nbsp;&nbsp;<img style="max-width:22px;" src="'.$this->session->user_image.'"/><b class="caret"></b></span></a>           
              <ul class="dropdown-menu">
                <li><a href="/index.php/User_controller/profiler">Perfil</a></li>                       
                <li><a href="/index.php/User_controller/closeSession">Cerrar Sesión</a></li>
              </ul>
            </li>';
            }?>
        </ul>
      </div>
    </div>
  </nav>

  <header class="masthead text-center d-flex text-white" id="header-register">
  <div class="container my-auto">
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="col-1 col-lg-3"></div>
            <div class="col-10 col-lg-6">
            <form id="form-register" action="<?=base_url()?>index.php/Product_controller/addProduct" method="post">
              <div class="form-row">
                <div class="col-1 col-lg-3"></div>
                <div class="form-group col col-lg">
                  <div class="col-lg mx-auto">
                    <h1 class="text-capitalize">
                      <strong>TaKn</strong>
                    </h1>
                    <h3>Agregar producto</h3>
                  </div>
                </div>
                <div class="col-1 col-lg-3"></div>
              </div>
              <div class="form-row">
                <div class="form-group col-12 col-lg">
                    <input id="input-name" type="text" class="form-control" name="input-name" placeholder="Nombre" max-length="100" required>
                  </div>
                  <div class="form-group col-12 col-lg">
                    <input id="input-category" type="text" class="form-control" name="input-category" placeholder="Categoria" max-length="100"
                      required>
                  </div>
              </div>              
              <div class="form-row">
                  <div class="form-group col-12 col-lg">
                    <input id="input-price" type="text" class="form-control" name="input-price" placeholder="Precio" max-length="100"
                      required>
                  </div>
              </div>
              <div class="form-row">
              <div class="form-group col-12 col-lg">
                    <input id="input-url" type="url" class="form-control" name="input-url" placeholder="Imagen(link)" max-length="100" required>
                  </div>                  
              </div>
              <div class="form-row">
              <div class="form-group col-6">
                    <select id="select-country" name="select-countries" class="form-control" required></select>
                  </div>
                  <div class="form-group col-6">
                    <select id="select-stock" name="select-stock" class="form-control" required>
                    <option value="Disponible">Disponible</option>
                    <option value="No disponible">No disponible</option>
                    </select>
                  </div>
              </div>
              <div class="form-row">
                <div class="form-group col">
                    <textarea name="textarea-description" id="textarea-description" class="form-control" rows="3"></textarea>
                </div>
              </div>                                          
            </div>            
            <div class="col-1 col-lg-3"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-2 col-lg-4"></div>
        <button type="submit" class="btn btn-light btn-xl js-scroll-trigger col-8 col-lg-4" href="#">Agregar</button>
        <div class="col-2 col-lg-4"></div>
      </div>
      </form>
    </div>
  </header>

  <section class="bg-primary" id="faq">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <h2 class="section-heading text-white">¿Dónde funciona TaKn?</h2>
          <br>
          <p class="text-faded mb-4">TaKn funciona en todo Chile*.</p>
          <small class="text-faded">*Excepto en zona insular y antártica.</small>
        </div>
      </div>
      <hr class="light my-4">
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <h2 class="section-heading text-white">¿Qué necesito para usar TaKn?</h2>
          <br>
          <p class="text-faded mb-4">Basta con susbscribir una forma de pago o cancelar en efectivo. Para lo demás, sólo necesitas un lugar desde donde
            solicitar nuestros servicios.</p>
        </div>
      </div>
      <hr class="light my-4">
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <h2 class="section-heading text-white">¿TaKn sólo tiene entrega a domicilio?</h2>
          <p class="text-faded mb-4">No, también tenemos un servicio de banquetería y de despacho en tienda. Nos importa que puedas disfrutar de TaKn
            lo más cerca posible de ti.</p>
        </div>
      </div>
    </div>
  </section>

  <section id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <h2 class="section-heading">¿Todavía tienes preguntas? Contáctanos</h2>
          <hr class="my-4">
          <p class="mb-5">Estamos disponibles para resolver tus dudas lo más pronto posible.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 ml-auto text-center">
          <i class="fa fa-phone fa-3x mb-3"></i>
          <p>300 800 5522&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;300 800 6699</p>
        </div>
        <div class="col-lg-4 mr-auto text-center">
          <i class="fa fa-envelope-open fa-3x mb-3"></i>
          <p>
            <a href="mailto:ayuda@takn.cl">ayuda@takn.cl</a>
          </p>
        </div>
      </div>
    </div>
  </section>

  <section id="footer" class="bg-dark text-white">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <h2 class="section-heading">Gracias por preferirnos</h2>
        </div>
      </div>
    </div>
  </section>

  <div class="container-fluid footer">
    <div class="row">
      <div class="container">
        <div class="row div-copyright-footer">
          <div class="col-lg text-center">
            <a href="/index.php/Welcome">TaKn</a>&nbsp;©&nbsp;2018&nbsp;-&nbsp;Todos los derechos reservados
            </p>
          </div>
        </div>
      </div>
    </div>
</body>

</html>