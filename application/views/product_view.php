<!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php if($product!==null){ echo
    '<title>TaKn | '.$product[0]['PRODUCT_NAME'].'</title>';
  }else{ echo
    '<title>Takn | Producto no encontrado</title>';
  }?>
  <link href="<?=base_url()?>resources/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
  <!-- Custom fonts -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  <link href="<?=base_url()?>resources/css/fontawesome-all.css" rel="stylesheet">
  <!-- Plugin CSS -->
  <link href="<?=base_url()?>resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
  <!-- Custom -->
  <link href="<?=base_url()?>resources/css/creative.css" rel="stylesheet">
  <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
  <!-- Bootstrap core JavaScript -->
  <script src="<?=base_url()?>resources/vendor/jquery/jquery.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Plugin JavaScript -->
  <script src="<?=base_url()?>resources/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/scrollreveal/scrollreveal.min.js"></script>
  <script src="<?=base_url()?>resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
  <!-- Custom scripts for this template -->
  <script defer src="<?=base_url()?>resources/js/creative.js"></script>
</head>

<body id="page-top">
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">TaKn<small class="nav-append">&nbsp;It's tasty</small></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
           <a class="nav-link js-scroll-trigger" href="/index.php/Welcome">INICIO</a>
          </li>
          <li class="nav-item">
           <a class="nav-link js-scroll-trigger" href="/index.php/User_controller/catalogue">CATÁLOGO</a>
          </li>          
          <li class="nav-item">
           <a class="nav-link js-scroll-trigger" href="#">|</a>
          </li>
          <?php if(strlen($this->session->id)<1){ echo
            '<li class="nav-item"><a class="nav-link js-scroll-trigger" href="/index.php/User_controller">INGRESAR</a></li>';
            }else{ echo
            '<li class="dropdown nav-drop nav-item">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown">'.$this->session->complete_name.'&nbsp;&nbsp;<img style="max-width:22px;" src="'.$this->session->user_image.'"/><b class="caret"></b></span></a>            
              <ul class="dropdown-menu" style="padding-top:20px;">
                <li class="divider"></li> 
                <li><a href="/index.php/User_controller/profiler">Perfil</a>';
                if($this->session->admin_prop == 2){ echo '
                  <li><a href="/index.php/User_controller/places">Mis Lugares</a></li>
                <li><a href="/index.php/User_controller/orders">Mis Órdenes</a></li>
                ';}echo '  
                <li><a href="/index.php/User_controller/closeSession">Cerrar Sesión</a></li>
              </ul>
            </li>';
            }?>
        </ul>
      </div>
    </div>
  </nav>

  <?php if ($product!==null){?>
  <header class="masthead text-center text-white d-flex">
   <div class="container my-auto" id="div-product">
    <div class="row" style="padding-top:60px; padding-bottom:60px;">
      <div class="col-12 col-lg-6">
        <img class="img-responsive" width="80%;" style="padding-bottom: 12px;" src="<?php echo $product[0]['PRODUCT_IMAGE'];?>"/>
      </div>
      <div class="col-12 col-lg-6">
        <h1 class="text-justify"><?php echo $product[0]['PRODUCT_NAME'];?></h1>
        <p class="text-justify">CLP&nbsp;<?php echo number_format($product[0]['PRODUCT_PRICE']);?></p>
        <p class="text-justify">Origen: <?php echo $product[0]['PRODUCT_ORIGIN']?></p>
        <p class="text-justify" style="max-width:90%;"><?php echo $product[0]['PRODUCT_DESCRIPTION'];?></p>
      </div>
    </div>        
   </div>   
  </header>

  <section id="comments" class="bg-dark text-white">
    <div class="container">
      <div class="row" style="background-color:#fff;color:#000;">
        <ul class="list-group col">
        <form id="form-add-comment" method="POST" action="<?=base_url();?>index.php/Product_controller/commentProduct/<?php echo $product[0]['PRODUCT_ID'];?>">
          <li class="list-group-item"><h3 class="text-left">Comenta este producto</h3></li>
          <li class="list-group-item"><textarea class="form-control" name="input-comment" id="input-comment" rows="4"></textarea></li>
          <li class="list-group-item"><button class="form-control btn btn-dark col col-lg" type="submit">Comentar</button></li>
        </form>
        </ul>
      </div>
      <br>
      <?php foreach($comments as $key => $cs){?>
        <div class="row" style="background-color:#fff;color:#000;">
          <ul class="list-group col">
            <li class="list-group-item">
              <h5 class="text-left"><?php echo $cs['T_USER_FIRSTNAME'].' '.$cs['T_USER_LASTNAME']?><h5>
              <small class="text-right">Publicado el&nbsp;<?php echo date($cs['T_USER_COMMENT_DATETIME'])?></small>              
            </li>
            <li class="list-group-item"><p class="col text-justify"><?php echo $cs['T_USER_COMMENT_CONTENT']?><p></li>
            <li class="list-group-item">
            <?php if($this->session->id == $cs['T_USER_ID'] || $this->session->admin_prop == 1){?>
              <form id="form-delete-comment" method="post" action="<?=base_url();?>index.php/User_controller/delComment">
                <input name="input-comment-id" value="<?php echo $cs['T_USER_COMMENT_ID'];?>" hidden/>
                <input name="input-product-id" value="<?php echo $cs['T_USER_COMMENT_PRODUCT_ID'];?>" hidden/>
                <button class="btn btn-dark" type="submit">Eliminar</button>
              </form>
            <?php }?>
            </li>
          </ul>
        </div>
        <br>
      <?php }?>
    </div>
  </section>
  <?php }else{?>
  <header class="masthead text-center text-white d-flex">
   <div class="container my-auto" id="div-product">
    <div class="row" style="padding-top:60px; padding-bottom:60px;">
      <div class="col col-lg">
        <h2 class="text-center">Lo sentimos, el producto solicitado no existe o no se encuentra disponible.</h2>        
      </div>
    </div>        
   </div>   
  </header>
  <?php }?>

  <section id="footer" class="bg-dark text-white">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <h2 class="section-heading">Gracias por preferirnos</h2>
        </div>
      </div>
    </div>
  </section>

  <div class="container-fluid footer">
    <div class="row">
      <div class="container">
        <div class="row div-copyright-footer">
          <div class="col-lg text-center">
            <a href="/index.php/Welcome">TaKn</a>&nbsp;©&nbsp;2018&nbsp;-&nbsp;Todos los derechos reservados
          </p>
        </div>
      </div>
    </div>
  </div>
</body>
</html>